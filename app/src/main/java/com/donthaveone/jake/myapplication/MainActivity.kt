package com.donthaveone.jake.myapplication

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast
import khttp.get
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.launch


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ToggleBtn.setOnClickListener{
            val text = "Hello toast!"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(applicationContext, text, duration)
            toast.show()
            sendCommand("test")
        }
    }


}

fun sendCommand(command: String) {
    launch {
        val command = """{"jsonrpc":"2.0","method":"Addons.ExecuteAddon","params":{"addonid":"script.json-cec","params":{"command":"activate"}},"id":1})"""

        val url = "http://192.168.0.30:8080/jsonrpc?request=" + command
        get(url, timeout=0.1)
    }

}
